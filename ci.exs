alias Buildah.{Cmd, Print}

{whoami_, 0} = System.cmd("whoami", [])
"root" = String.trim(whoami_)
{_, 0} = Cmd.version(into: IO.stream(:stdio, :line))
{_, 0} = Cmd.info(into: IO.stream(:stdio, :line))

{parsed, args, invalid} = OptionParser.parse(System.argv(), strict: [tag: :string])
IO.puts(invalid)

OciNixAlpine.make_alpine_images(
    "docker.io",
    "alpine",
    "docker://" <> System.fetch_env!("CI_REGISTRY_IMAGE"),
    parsed
)

Print.images()

