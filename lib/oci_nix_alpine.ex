alias Buildah.Nix.{
    Ghc.Sqlite
}

alias Buildah.Apk.{
    Catatonit,
    Moreutils,
    Nix,
    Sqlite,
    Sudo
}

alias ContainerImageUtil

defmodule OciNixAlpine do

    def make_alpine_images(
            from_registry,
            from_name,
            to_registry,
            options \\ []
        ) do
        options = Keyword.merge([
                tag: nil #,
                # repositories_to_add: []
            ],
            options
        )
        tag = options[:tag]
        options = Keyword.merge([from_tag: tag], options)
        from_tag = options[:from_tag]
        # repositories_to_add = options[:repositories_to_add]
        quiet = System.get_env("QUIET")

        # alpine/catatonit
        Buildah.from_push(
            ContainerImageUtil.image_name(
                from_registry, from_name, from_tag),
            &Catatonit.on_container/2,
            &Catatonit.test/3,
            image_to: ContainerImageUtil.image_name(
                to_registry, "catatonit", tag),
            quiet: quiet
        )

        # alpine/moreutils
        Buildah.from_push(
            ContainerImageUtil.image_name(
                to_registry, "catatonit", tag),
            &Moreutils.on_container/2,
            &Moreutils.test/3,
            image_to: ContainerImageUtil.image_name(
                to_registry, "moreutils", tag),
            quiet: quiet
        )

        # alpine/sudo
        Buildah.from_push(
            ContainerImageUtil.image_name(
                to_registry, "moreutils", tag),
            &Sudo.on_container_adding_user_user/2,
            &Sudo.test/3,
            image_to: ContainerImageUtil.image_name(
                to_registry, "sudo", tag),
            quiet: quiet
        )

        # alpine/sqlite
        Buildah.from_push(
            ContainerImageUtil.image_name(
                to_registry, "sudo", tag),
            &Sqlite.on_container/2,
            &Sqlite.test/3,
            image_to: ContainerImageUtil.image_name(
                to_registry, "sqlite", tag),
            quiet: quiet
        )

        # alpine/nix
        Buildah.from_push(
            ContainerImageUtil.image_name(
                to_registry, "sqlite", tag),
            &Nix.on_container/2,
            &Nix.test/3,
            image_to: ContainerImageUtil.image_name(
                to_registry, "nix", tag),
            quiet: quiet
        )

        # nix-alpine/ghc Buildah.Nix.Ghc.Sqlite
        Buildah.from_push(
            ContainerImageUtil.image_name(
                to_registry, "nix", tag),
            &Buildah.Nix.Ghc.Sqlite.on_container/2,
            &Buildah.Nix.Ghc.Sqlite.test/3,
            image_to: ContainerImageUtil.image_name(
                to_registry, "ghc", tag),
            quiet: quiet
        )
  end

end

